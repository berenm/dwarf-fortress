/**
 * @file
 * @date 13 juin 2010
 * @todo comment
 */

#ifndef INTEGER_TYPES_HPP_
#define INTEGER_TYPES_HPP_

#include <boost/integer.hpp>

using ::boost::int8_t;
using ::boost::int16_t;
using ::boost::int32_t;
using ::boost::int64_t;

using ::boost::int_fast8_t;
using ::boost::int_fast16_t;
using ::boost::int_fast32_t;
using ::boost::int_fast64_t;

using ::boost::int_least8_t;
using ::boost::int_least16_t;
using ::boost::int_least32_t;
using ::boost::int_least64_t;

using ::boost::uint8_t;
using ::boost::uint16_t;
using ::boost::uint32_t;
using ::boost::uint64_t;

using ::boost::uint_fast8_t;
using ::boost::uint_fast16_t;
using ::boost::uint_fast32_t;
using ::boost::uint_fast64_t;

using ::boost::uint_least8_t;
using ::boost::uint_least16_t;
using ::boost::uint_least32_t;
using ::boost::uint_least64_t;

#endif /* INTEGER_TYPES_HPP_ */
