/**
 * @file
 * @date 14 juin 2010
 * @todo comment
 */

#ifndef INTERN_ENABLER_INPUT_REGISTERED_KEY_HPP_
#define INTERN_ENABLER_INPUT_REGISTERED_KEY_HPP_

#include <string>

#include "input.hpp"

struct registered_key {
    match_type type;
    ::std::string display;
};

#endif /* INTERN_ENABLER_INPUT_REGISTERED_KEY_HPP_ */
