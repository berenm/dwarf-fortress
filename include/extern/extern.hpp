/**
 * @file
 * @date 27 juin 2010
 * @todo comment
 */

#ifndef EXTERN_HPP_
#define EXTERN_HPP_

/**
 * @defgroup extern External code
 * @brief Contains external declarations and internal definitions of structures, methods and functions.
 * @details External declaration means that the structures, methods or functions declared are visible
 *   from outside libgraphics, and are used by the dwarf binary, and so, should not be modified. External definition
 *   means that the structures, methods or functions are used by the dwarf binary, and their detail may be rewritten,
 *   as long as they have the same behavior.
 */

#endif /* EXTERN_HPP_ */
