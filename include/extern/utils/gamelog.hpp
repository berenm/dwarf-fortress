/**
 * @file
 * @date 6 juin 2010
 * @todo comment
 */

#ifndef EXTERN_UTILS_GAMELOG_HPP_
#define EXTERN_UTILS_GAMELOG_HPP_

#include <string>

/**
 * @{
 * @ingroup extern
 */
void errorlog_string(const char*);
void errorlog_string(const ::std::string&);
void gamelog_string(const ::std::string&);
/** @} */

#endif /* EXTERN_UTILS_GAMELOG_HPP_ */
