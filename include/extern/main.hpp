/**
 * @file
 * @date 27 juin 2010
 * @todo comment
 */

#ifndef EXTERN_MAIN_HPP_
#define EXTERN_MAIN_HPP_

/**
 * @ingroup extern
 */
int main(int argc, char* argv[]);

#endif /* EXTERN_MAIN_HPP_ */
