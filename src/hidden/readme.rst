src/hidden
============

This folder contains definitions of structures that the library has to use, but which are compiled in dwarf fortress closed binary.
We cannot change anything to these, they aren't even build with the library as they are available and compiled in the closed binary.